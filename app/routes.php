<?php

use \Slim\Http\Request;
use \Slim\Http\Response;

$app->get('/', function () {
  return 'Silence is golden!';
});

$app->group('/v{version:[0-9]+}/{device:[0-9]+}', function () {
  $this->post('/leaderboards/get', function (Request $request, Response $response, $args) {

    // 0. Prepare response.
    $data = array(
      'error'     => 0,
      'timestamp' => time()
    );

    // 1. Configure service call.
    try {
      $params       = array_merge($request->getParams(), $args);
      $call         = new \NNAPI\Calls\LeaderboardGet($params);
      $data['data'] = $call->proceed();
    } catch (\NNAPI\Exception $e) {

      $data['error']         = 1;
      $data['error_code']    = $e->getCode();
      $data['error_message'] = $e->getMessage();
    }

    return $response->withJson($data);
  });

  $this->post('/leaderboards/standing', function (Request $request, Response $response, $args) {

    // 0. Prepare response.
    $data = array(
      'error'     => 0,
      'timestamp' => time()
    );

    // 1. Configure service call.
    try {
      $params       = array_merge($request->getParams(), $args);
      $call         = new \NNAPI\Calls\LeaderboardStanding($params);
      $data['data'] = $call->proceed();
    } catch (\NNAPI\Exception $e) {

      $data['error']         = 1;
      $data['error_code']    = $e->getCode();
      $data['error_message'] = $e->getMessage();
    }

    return $response->withJson($data);
  });
});