<?php namespace NNAPI;

use NNCore\Auth;
use NNAPI\Calls\CallInterface;
use NNCore\Tools\Tool;

/**
 * Class CallAbstract
 *
 * @package NNAPI
 */
abstract class CallAbstract implements CallInterface {

  /**
   * Min version which supports the call.
   *
   * @var int
   */
  protected $_min_version = 7;

  /**
   * Is authentication required?
   *
   * @var bool
   */
  protected $_auth = TRUE;

  /**
   * Locale.
   *
   * @var string
   */
  protected $_locale = 'en';

  /**
   * List of the params sent via API.
   *
   * @var array
   */
  protected $_params = array();

  /**
   * List of mandatory params.
   *
   * @var array
   */
  protected $_mandatory_params = array(
    'uid',
    'pw',
    'payment_provider_id',
    'app_version',
    'version',
    'device'
  );

  /**
   * Return of the call.
   *
   * @var array
   */
  protected $_return = array();

  /**
   * CallAbstract constructor.
   *
   * @param array $params
   *
   * @throws Exception
   */
  public function __construct(array $params = array()) {

    // 1. Get columns we need.
    $this->_params = $params + $this->_params;

    // 2. If we need to check mandatory params, do it.
    if (!empty($this->_mandatory_params) && $this->_check_mandatory_params() === FALSE) {
      throw new Exception('At least one of mandatory params is missing.', Exception::CALL_MISSING_PARAM);
    }

    // 3. Check min service version.
    if ($this->_min_version > $this->_params['version']) {
      throw new Exception('Your service version is not supported.', Exception::CALL_VERSION_NOT_SUPPORTED);
    }

    // 4. If authentication is require, do it.
    if ($this->_auth && $this->_authenticate_user() === FALSE) {
      throw new Exception('You do not have permission to proceed.', Exception::CALL_INVALID_CREDENTIALS);
    }

    // 5. Set locale if possible.
    if (!empty($params['locale'])) {
      $this->set_locale($params['locale']);
    }
  }

  /**
   * Sets locale.
   *
   * @param $locale
   */
  public function set_locale($locale) {
    $this->_locale = Tool::fix_locale($locale);
  }

  /**
   * Authenticate user.
   *
   * @return bool
   */
  protected function _authenticate_user() {
    return (bool) Auth::authenticate($this->_params['uid'], $this->_params['pw']);
  }

  /**
   * Checks all mandatory params are sent.
   *
   * @throws Exception
   */
  protected function _check_mandatory_params() {
    if (empty($this->_mandatory_params)) {
      return TRUE;
    }

    foreach ($this->_mandatory_params as $params) {
      if (!array_key_exists($params, $this->_params)) {
        RETURN FALSE;
      }
    }

    return TRUE;
  }
}