<?php namespace NNAPI;

/**
 * Class Exception
 *
 * @package NNAPI
 */
class Exception extends \Exception {
  const CALL_INVALID_CREDENTIALS   = 1;
  const CALL_MISSING_PARAM         = 2;
  const CALL_VERSION_NOT_SUPPORTED = 3;
  const CALL_INVALID_VALUE         = 4;
}