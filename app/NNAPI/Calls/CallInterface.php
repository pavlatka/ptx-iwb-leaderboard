<?php namespace NNAPI\Calls;

/**
 * Interface CallInterface
 *
 * @package NNAPI\Calls
 */
interface CallInterface {

  /**
   * Process the call and returns results.
   *
   * @return array
   */
  public function proceed();
}