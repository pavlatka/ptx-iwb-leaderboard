<?php namespace NNAPI\Calls;

use NNAPI\CallAbstract;
use NNAPI\Exception;
use NNCore\Cache;
use NNCore\User;
use NNLeaderboard\Collections\RunsCollection;
use NNLeaderboard\Collections\UserTrophiesCollection;
use NNLeaderboard\Run;
use NNLeaderboard\RunGroup;

/**
 * Class LeaderboardStanding
 *
 * @package NNAPI\Calls
 */
class LeaderboardStanding extends CallAbstract {

  /**
   * Cache limit for standing in seconds
   */
  const CACHE_STANDING_LIVE_TIME = 60;

  /**
   * LeaderboardStanding constructor.
   *
   * @param array $params
   *
   * @throws Exception
   */
  public function __construct(array $params) {

    // 1. We need 2 more params.
    $this->_mandatory_params[] = 'run_id';
    $this->_mandatory_params[] = 'leaderboard_group_id';

    // 2. Call parent constructor.
    parent::__construct($params);

    // 3. Check run exists and belongs to leaderboard.
    if ($this->_check_run_leaderboard() === FALSE) {
      throw new Exception('Run does exists or does not belong to leaderboard', Exception::CALL_INVALID_VALUE);
    }
  }

  /**
   * Process the call
   */
  public function proceed() {

    // 1. Variables.
    $uid      = $this->_params['uid'];
    $run_id   = $this->_params['run_id'];
    $group_id = $this->_params['leaderboard_group_id'];

    // 2. Try to check cache.
    $cache_key  = 'lb_standing_' . $run_id . '_' . $group_id;
    $cache_data = Cache::get($cache_key);
    if (!empty($cache_data)) {
      return (array) unserialize($cache_data);
    }

    // 3. Get standing.
    $run_group = new RunGroup($run_id, $group_id);
    $standing  = $run_group->get_standing();

    // 4. Try to figure out position of the user.
    $this->_return['user_standing'] = array(
      'uid'    => $uid,
      'run_xp' => $run_group->run->get_xp_4_uid($uid)
    );
    if (array_key_exists($uid, $standing)) {
      $this->_return['user_standing']['position'] = array_search($uid, array_keys($standing));
    }
    else {
      $this->_return['user_standing']['position'] = $run_group->get_user_approx_position($uid);
    }

    // 5. Complete profiles.
    $users_uids     = array_merge(array_keys($standing), array((int) $uid));
    $users_profiles = User::get_profile_4_uids($users_uids, array(), array('name', 'real_name'));
    foreach ($users_profiles as $uid => &$profile) {
      $profile['trophies'] = UserTrophiesCollection::get_4_uid($uid);
    }

    // 6. Complete data in standing.
    $this->_return['standing'] = array();
    foreach ($standing as $uid => $value) {
      $this->_return['standing'][] = array_merge($value->toArray(), $users_profiles[$uid]);
    }

    // 7. Complete data for user_standing.
    $this->_return['user_standing'] += $users_profiles[$uid];

    // 8. Save data into cache.
    Cache::set($cache_key, serialize($this->_return), self::CACHE_STANDING_LIVE_TIME);

    // 9. Return what we have found.
    return (array) $this->_return;
  }

  /**
   * Checks whether run / leaderboard exists and
   * are connected.
   */
  private function _check_run_leaderboard() {
    try {
      $run = new Run($this->_params['run_id']);
      if (RunsCollection::run_has_group($run->get_id(), $this->_params['leaderboard_group_id']) === FALSE) {
        return FALSE;
      }
    } catch (\NNLeaderboard\Exception $e) {
      return FALSE;
    }
  }
}