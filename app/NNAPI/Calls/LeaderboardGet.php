<?php namespace NNAPI\Calls;

use NNAPI\CallAbstract;
use NNLeaderboard\Collections\UserRunsCollection;

/**
 * Class LeaderboardGet
 *
 * @package NNAPI\Calls
 */
class LeaderboardGet extends CallAbstract {

  /**
   * Process the call
   */
  public function proceed() {
    $collection = new UserRunsCollection($this->_params['uid'], $this->_locale, $this->_params);

    return $collection->get_active_runs();
  }
}