<?php namespace NNLeaderboard;

/**
 * Class Localization
 *
 * @package NNLeaderboard
 */
class Localization {

  /**
   * Deletes all records for the certain parent type.
   *
   * @param string      $type
   * @param int         $id
   * @param string|null $locale - possible to delete data only for certain locale
   *
   * @return bool
   */
  public static function delete($type, $id, $locale = NULL) {
    return \dibi::query('
      DELETE FROM [nn_leaderboard_localizations]
      WHERE
        [localization_type] = %s
        AND [localization_id] = %i
      ', $type, $id,
      '%if', !empty($locale), ' AND [locale] = %s', $locale);
  }

  /**
   * Returns data for certain locale.
   *
   * @param string $localization_type - type of localization entry
   * @param int    $localization_id   - ID of localization entry
   * @param string $locale            - requested locale
   *
   * @return string|NULL
   */
  public static function get($localization_type, $localization_id, $locale) {
    $result = \dibi::query('
      SELECT [content] 
      FROM [nn_leaderboard_localizations]
      WHERE
        [localization_type] = %s 
        AND [localization_id] = %i 
        AND [locale] = %s
      LIMIT 1;
    ', $localization_type, $localization_id, $locale);

    return $result->fetchSingle();
  }

  /**
   * Updates localization for the leaderboard group.
   *
   * @param int   $group_id - ID of the group
   * @param array $names    - names in different locales
   *
   * @return bool
   */
  public static function update_4_group($group_id, array $names = array()) {
    // 1. If we do not have names, just delete translations.
    self::delete('group', $group_id);

    // 2. Update locale data.
    $save_data = array();
    foreach ($names as $locale => $text) {
      $save_data[] = array(
        'localization_type' => 'group',
        'localization_id'   => $group_id,
        'locale'            => $locale,
        'content'           => $text
      );
    }
    \dibi::query('INSERT INTO [nn_leaderboard_localizations] %ex', $save_data);

    return TRUE;
  }
}