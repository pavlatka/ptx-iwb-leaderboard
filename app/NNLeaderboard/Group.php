<?php namespace NNLeaderboard;

/**
 * Class Group
 *
 * @package NNLeaderboard
 */
class Group {

  /**
   * @var
   */
  private $_id;

  /**
   * @var
   */
  private $_data;

  /**
   * Group constructor.
   *
   * @param int $id - ID of the leaderboard
   *
   * @throws Exception
   */
  public function __construct($id) {
    $this->_id = $id;
    $this->_load_data();
  }

  /**
   * Return ID of the group.
   *
   * @return int
   */
  public function get_id() {
    return (int) $this->_id;
  }

  /**
   * Loads data about the group.
   *
   * @throws Exception
   */
  private function _load_data() {
    $result      = \dibi::query('
      SELECT *
      FROM [nn_leaderboard_groups]
      WHERE 
        [group_id] = %i
    ', $this->_id);
    $this->_data = $result->fetch();

    if (empty($this->_data)) {
      throw new Exception('Group - ' . $this->_id . ' - cannot be found');
    }
  }
}