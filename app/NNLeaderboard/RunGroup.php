<?php namespace NNLeaderboard;

/**
 * Class RunGroup
 *
 * @package NNLeaderboard
 */
class RunGroup {

  /**
   * @var Run
   */
  public $run;

  /**
   * @var Group
   */
  public $group;

  /**
   * RunGroup constructor.
   *
   * @param $run_id
   * @param $group_id
   */
  public function __construct($run_id, $group_id) {
    $this->run   = new Run($run_id);
    $this->group = new Group($group_id);
  }

  /**
   * Returns number of users in certain group and run.
   *
   * @return int
   */
  public function count_users() {
    $result = \dibi::query('
      SELECT IFNULL(COUNT(*), 0)
      FROM [nn_leaderboard_group_run_users] ru 
      JOIN [nn_leaderboard_run_users] u USING(uid, run_id)
      JOIN [node] n ON 
        n.[type] = "profile" 
        AND n.[uid] = u.uid
      JOIN [content_type_profile] p USING(nid, vid)
      WHERE 
        ru.[group_id] = %i 
        AND ru.[run_id] = %i 
        AND p.[field_realname_value] IS NOT NULL
        AND p.[field_realname_value] != ""
        AND p.[field_realname_value] NOT IN %l
    ', $this->group->get_id(), $this->run->get_id(), Leaderboard::$exclude_names);

    return (int) $result->fetchSingle();
  }

  /**
   * Returns current standing.
   *
   * @return array
   */
  public function get_standing() {
    $result = \dibi::query('
      SELECT
        u.[uid], u.[run_xp]
      FROM [nn_leaderboard_run_users] u
      JOIN [nn_leaderboard_group_run_users] ru USING (run_id, uid)
      JOIN [node] n ON 
        n.[type] = "profile" 
        AND n.[uid] = u.uid
      JOIN [content_type_profile] p USING(nid, vid)
      WHERE
        ru.[group_id] = %i
        AND u.[run_id] = %i 
        AND u.[run_xp] >= %i
        AND p.[field_realname_value] IS NOT NULL
        AND p.[field_realname_value] != ""
        AND p.[field_realname_value] NOT IN %l
      GROUP BY [uid]
      ORDER BY 
        u.[run_xp] DESC, 
        u.[last_update] DESC
      %lmt
    ', $this->group->get_id(), $this->run->get_id(),
      $this->run->leaderboard->get_min_score(), Leaderboard::$exclude_names,
      $this->run->leaderboard->get_max_users());

    return (array) $result->fetchAssoc('uid');
  }

  /**
   * Approximate standing position of the user in the leaderboard.
   * If you need exact number, it's better to user self::get_uid_standing function instead.
   *
   * @param int $uid - UID of the user
   *
   * @return int
   */
  public function get_user_approx_position($uid) {
    // 1. Prepare query.
    $result = \dibi::query('
      SELECT * FROM (
        SELECT 
          @rank:=@rank+1 AS rank, 
          u.* 
        FROM [nn_leaderboard_run_users] u
        WHERE 
          u.[run_id] = %i
          AND uid IN (
            SELECT _ru.[uid]
            FROM [nn_leaderboard_group_run_users] _ru
            JOIN [nn_leaderboard_groups] _g USING (group_id)
            JOIN [node] _n ON 
              _n.[type] = "profile" AND 
              _n.[uid] = _ru.uid
            JOIN [content_type_profile] _p USING(nid, vid)
            WHERE  
              _g.[group_id] = %i
              AND _ru.[run_id] = %i
              AND _p.[field_realname_value] IS NOT NULL
              AND _p.[field_realname_value] != ""
              AND _p.[field_realname_value] NOT IN %l
          )
        ORDER BY u.[run_xp] DESC, u.[last_update] ASC
      ) t, (SELECT @rank:=0) t2
      WHERE [uid] = %i
      %lmt
    ', $this->run->get_id(), $this->group->get_id(), $this->run->get_id(), Leaderboard::$exclude_names, $uid, 1);

    // 2. Try to get data from the run.
    $rank = $result->fetchSingle();
    if (!empty($rank)) {
      return (int) $rank;
    }

    // 3. User is not part of the leaderboard, so he's on the last position.
    return $this->count_users() + 1;
  }
}