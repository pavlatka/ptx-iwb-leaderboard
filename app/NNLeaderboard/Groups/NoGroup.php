<?php namespace NNLeaderboard\Groups;

/**
 * Class NoGroup
 *
 * @package NNLeaderboard\Groups
 */
class NoGroup extends GroupAbstract {

  /**
   * Returns list of the groups the user is assigned to.
   *
   * @param int $uid - UID of the user§
   *
   * @return mixed
   */
  public function get_4_uid($uid) {
    return array(
      array(
        'active'       => 1,
        'weight'       => 0,
        'name'         => 'All Users',
        'code'         => self::count_group_code('all'),
        'trophy_group' => 1,
        'localization' => array(
          'de' => 'All Users',
          'ru' => 'All Users',
          'es' => 'All Users',
          'fr' => 'All Users',
          'it' => 'All Users',
          'pt' => 'All Users',
          'tr' => 'All Users'
        )
      )
    );
  }

}