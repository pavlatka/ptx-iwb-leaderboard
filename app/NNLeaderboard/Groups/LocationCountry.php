<?php namespace NNLeaderboard\Groups;
use NNCore\Tools\IpTool;

/**
 * Class LocationCountry
 *
 * @package NNLeaderboard\Groups
 */
class LocationCountry extends GroupAbstract {

  /**
   * Returns list of the groups the user is assigned to.
   *
   * @param int $uid - UID of the user§
   *
   * @return mixed
   */
  public function get_4_uid($uid) {
    // 1. Prepare main group everyone belongs to.
    $groups = array(
      array(
        'active'       => 1,
        'weight'       => 0,
        'name'         => 'World wide',
        'code'         => self::count_group_code('country_all'),
        'trophy_group' => 1,
        'img_url'      => 'https://nnapp.s3.amazonaws.com/enosis/world.png',
        'img_width'    => 750,
        'img_height'   => 320,
        'localization' => array(
          'de' => 'Weltweit',
          'ru' => 'в мире',
          'es' => 'Mundial',
          'fr' => 'Mondial',
          'it' => 'Mondiale',
          'pt' => 'Mundial',
          'tr' => 'Dünya çapinda'
        )
      )
    );

    // 2. Load data base on the IP.
    $geo_data = IpTool::get_geo_data();
    if (!empty($geo_data)) {
      if (!empty($geo_data->country_name) && $geo_data->country_name != '-') {
        $groups[] = array(
          'active'       => 0,
          'weight'       => 10,
          'name'         => $geo_data->country_name,
          'code'         => self::count_group_code('country_' . strtolower($geo_data->country_code)),
          'trophy_group' => 2,
          'img_url'      => 'https://nnapp.s3.amazonaws.com/enosis/country.png',
          'img_width'    => 750,
          'img_height'   => 320,
          'localization' => array(
            'de' => IpTool::get_localize_name('country', $geo_data->country_code, 'DE'),
            'ru' => IpTool::get_localize_name('country', $geo_data->country_code, 'RU'),
            'es' => IpTool::get_localize_name('country', $geo_data->country_code, 'ES'),
            'fr' => IpTool::get_localize_name('country', $geo_data->country_code, 'FR'),
            'it' => IpTool::get_localize_name('country', $geo_data->country_code, 'IT'),
            'pt' => IpTool::get_localize_name('country', $geo_data->country_code, 'PT'),
            'tr' => IpTool::get_localize_name('country', $geo_data->country_code, 'TR')
          )
        );
      }

      if (date('Y-m-d') >= '2016-05-30') {
        if (!empty($geo_data->region_name) && $geo_data->region_name != '-') {
          $groups[] = array(
            'active'       => 0,
            'weight'       => 20,
            'name'         => $geo_data->region_name,
            'code'         => self::count_group_code('region_' . strtolower($geo_data->region_name)),
            'trophy_group' => 3,
            'img_url'      => 'https://nnapp.s3.amazonaws.com/enosis/city.png',
            'img_width'    => 750,
            'img_height'   => 320,
            'localization' => array(
              'de' => IpTool::get_localize_name('region', $geo_data->region_name, 'DE'),
              'ru' => IpTool::get_localize_name('region', $geo_data->region_name, 'RU'),
              'es' => IpTool::get_localize_name('region', $geo_data->region_name, 'ES'),
              'fr' => IpTool::get_localize_name('region', $geo_data->region_name, 'FR'),
              'it' => IpTool::get_localize_name('region', $geo_data->region_name, 'IT'),
              'pt' => IpTool::get_localize_name('region', $geo_data->region_name, 'PT'),
              'tr' => IpTool::get_localize_name('region', $geo_data->region_name, 'TR')
            )
          );
        }
      }
      elseif (!empty($geo_data->city_name) && $geo_data->city_name != '-') {
        $groups[] = array(
          'active'       => 0,
          'weight'       => 20,
          'name'         => $geo_data->city_name,
          'code'         => self::count_group_code('city_' . strtolower($geo_data->city_name)),
          'trophy_group' => 3,
          'img_url'      => 'https://nnapp.s3.amazonaws.com/enosis/city.png',
          'img_width'    => 750,
          'img_height'   => 320,
          'localization' => array(
            'de' => IpTool::get_localize_name('city', $geo_data->city_name, 'DE'),
            'ru' => IpTool::get_localize_name('city', $geo_data->city_name, 'RU'),
            'es' => IpTool::get_localize_name('city', $geo_data->city_name, 'ES'),
            'fr' => IpTool::get_localize_name('city', $geo_data->city_name, 'FR'),
            'it' => IpTool::get_localize_name('city', $geo_data->city_name, 'IT'),
            'pt' => IpTool::get_localize_name('city', $geo_data->city_name, 'PT'),
            'tr' => IpTool::get_localize_name('city', $geo_data->city_name, 'TR')
          )
        );
      }
    }

    return (array) $groups;
  }
}