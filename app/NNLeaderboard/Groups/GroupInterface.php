<?php namespace NNLeaderboard\Groups;

/**
 * Interface GroupInterface
 *
 * @package NNLeaderboard\Groups
 */
interface GroupInterface {

  /**
   * Returns list of the groups the user is assigned to.
   *
   * @param int $uid - UID of the user§
   *
   * @return mixed
   */
  public function get_4_uid($uid);
}