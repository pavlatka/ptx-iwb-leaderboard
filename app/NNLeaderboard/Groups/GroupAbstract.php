<?php namespace NNLeaderboard\Groups;

use NNLeaderboard\Leaderboard;
use NNLeaderboard\Localization;
use NNLeaderboard\Run;

/**
 * Class GroupAbstract
 *
 * @package NNLeaderboard\Groups
 */
abstract class GroupAbstract implements GroupInterface {

  /**
   * Links user to one of the group and returns their IDs
   *
   * @param int $uid    - UID of the user
   * @param int $run_id - ID of the run
   *
   * @return array
   */
  public function assign_to_group($uid, $run_id) {
    // 1. Try to find in existing one.
    $group_ids = $this->_get_current_group_id($uid, $run_id);
    if (FALSE && !empty($group_ids)) {
      return array_keys($group_ids);
    }

    // 2. Find the group_id the user should belong to.
    $groups_data = $this->get_4_uid($uid);
    $groups_ids  = $this->save_groups($run_id, $groups_data);

    // 3. Save new relations.
    if (!empty($groups_ids)) {
      $save_data = array();
      foreach ($groups_ids as $group_id) {
        $save_data[] = array(
          'uid'      => $uid,
          'run_id'   => $run_id,
          'group_id' => $group_id
        );
      }

      \dibi::query('
        INSERT IGNORE INTO [nn_leaderboard_group_run_users] %ex', $save_data);
    }

    // 3. Return ID of the group.
    return (array) $groups_ids;
  }

  /**
   * Counts help code for the group.
   *
   * @param string $code
   *
   * @return string
   */
  public static function count_group_code($code) {
    return substr(sha1($code), 0, 7);
  }

  /**
   * Checks whether uid belongs to this group or not.
   *
   * @param int $uid    - UID of the user
   * @param int $run_id - ID of the run
   *
   * @return bool
   */
  public function has_group($uid, $run_id) {
    $group_ids = $this->assign_to_group($uid, $run_id);

    return !empty($group_ids);
  }

  /**
   * Returns active groups for the run and user.
   *
   * @param int    $run_id       - ID of the run
   * @param int    $uid          - UID of the user
   * @param bool   $assign_first - should we first assign users to the groups
   * @param bool   $recount      - should we recount group_active before returning data
   * @param string $locale       - possible to ask for names in different locale
   *
   * @return array
   */
  public function get_active_groups($run_id, $uid, $assign_first = FALSE, $recount = TRUE, $locale = NULL) {
    // 1. If we should first assign users to the groups, do it.
    if ($assign_first) {
      $this->assign_to_group($uid, $run_id);
    }

    // 2. Check activation of the groups?
    if ($recount) {
      $this->update_group_active($uid, $run_id);
    }

    // 3. Query.
    $result = \dibi::query('
      SELECT 
        g.[group_id] id, 
        g.[name], 
        g.[group_weight],
        g.[trophy_group],
        g.[image_url], 
        g.[image_width], 
        g.[image_height]
      FROM [nn_leaderboard_group_run_users] u
      JOIN [nn_leaderboard_group_runs] r USING(group_id, run_id)
      JOIN [nn_leaderboard_groups] g USING(group_id)
      WHERE
        u.[uid] = %i 
        AND u.[run_id] = %i 
        AND r.[group_active] = %i
      ORDER BY g.[group_weight] DESC
    ', $uid, $run_id, 1);

    // 4. Collect data.
    $active_groups = array();
    foreach ($result->getIterator() as $data) {
      $name     = $data->name;
      $group_id = $data->id;
      if ($locale !== NULL) {
        $locale_name = Localization::get('group', $group_id, $locale);
        if (!empty($locale_name)) {
          $name = $locale_name;
        }
      }

      $group_data               = $data->toArray() + array('name' => $name);
      $active_groups[$group_id] = $group_data;
    }

    return (array) $active_groups;
  }

  /**
   * Saves relations between group and run
   *
   * @param int   $run_id - ID of the run the groups are for
   * @param array $groups - array of the groups to be saved.
   *
   * @return array
   */
  public function save_groups($run_id, $groups) {

    // 1. Prepare data for further use.
    $codes     = array();
    $save_data = array();
    foreach ($groups as $group) {
      $save_data[] = array(
        'name'         => $group['name'],
        'group_code'   => $group['code'],
        'group_weight' => $group['weight'],
        'trophy_group' => $group['trophy_group'],
        'group_active' => $group['active'],
        'image_url'    => !empty($group['image_url']) ? $group['image_url'] : NULL,
        'image_width'  => !empty($group['image_width']) ? $group['image_width'] : 0,
        'image_height' => !empty($group['image_height']) ? $group['image_height'] : 0
      );

      $codes[] = $group['code'];
    }

    // 2. Insert groups to the table.
    \dibi::query('INSERT IGNORE INTO [nn_leaderboard_groups] %ex', $save_data);

    // 3. Select data of newly created groups.
    $sql         = '
      SELECT [group_id], [group_active], [group_code]
      FROM [nn_leaderboard_groups]
      WHERE [group_code] IN %l
    ';
    $result      = \dibi::query($sql, $codes);
    $groups_data = $result->fetchAssoc('group_code');

    // 4. Make sure groups are linked with run.
    $save_data = [];
    foreach ($groups_data as $group) {
      $save_data[] = array(
        'group_id'     => $group->group_id,
        'run_id'       => $run_id,
        'group_active' => $group->group_active,
      );
    }
    \dibi::query('INSERT IGNORE INTO [nn_leaderboard_group_runs] %ex', $save_data);

    // 5. Update localization for groups.
    foreach ($groups as $group) {
      $group_code = $group['code'];
      if (!array_key_exists('localization', $group) || empty($groups_data[$group_code])) {
        continue;
      }

      Localization::update_4_group($groups_data[$group_code]->group_id, $group['localization']);
    }

    // 6. Build ids array.
    $group_ids = array();
    foreach ($groups_data as $group) {
      $group_ids[] = (int) $group->group_id;
    }

    // 7. Return it.
    return $group_ids;
  }

  /**
   * Checks all the groups for certain run and makes sure that column
   * group_active has correct value.
   *
   * @param int $uid    - UID of the user
   * @param int $run_id - ID of run
   *
   * @return bool
   */
  public function update_group_active($uid, $run_id) {

    // 1. Load groups which are not active yet.
    $result = \dibi::query('
      SELECT [group_id]
      FROM [nn_leaderboard_group_run_users] u
      JOIN [nn_leaderboard_group_runs] r USING(group_id, run_id)
      WHERE 
        u.[uid] = %i 
        AND u.[run_id] = %i 
        AND r.[group_active] = %i
    ', $uid, $run_id, 0);
    $groups = $result->fetchAll();
    if (empty($groups)) {
      return TRUE;
    }

    // 2. Prepare SQL template for further use.
    $sql_tpl = '
      SELECT COUNT(*) count
      FROM [nn_leaderboard_run_users] u
      JOIN [nn_leaderboard_group_run_users] gu USING (uid, run_id)
      JOIN [node] n ON n.type = "profile" AND n.uid = u.uid
      JOIN [content_type_profile] p USING(nid, vid)
      WHERE 
        u.[run_id] = %i 
        AND gu.[group_id] = %i 
        AND u.[run_xp] >= %i
        AND p.[field_realname_value] IS NOT NULL
        AND p.[field_realname_value] != ""
        AND p.[field_realname_value] NOT IN %l
    ';

    // 3. Let's check each group separately
    $run                   = new Run($run_id);
    $activate_groups       = array();
    $leaderboard_min_score = $run->leaderboard->get_min_score();
    foreach ($groups as $group) {
      $result        = \dibi::query($sql_tpl, $run_id, $group->group_id, $leaderboard_min_score, Leaderboard::$exclude_names);
      $count_players = $result->fetchSingle();
      if ($count_players >= $run->leaderboard->get_min_users()) {
        $activate_groups[] = $group->group_id;
      }
    }

    // 4. If we do not have anything to activate, we are done.
    if (empty($activate_groups)) {
      return TRUE;
    }

    // 5. Activate what has to be activated
    return (bool) \dibi::query('
      UPDATE [nn_leaderboard_group_runs]',
      array('group_active' => 1),
      'WHERE
        [run_id] = %i
        AND [group_id] IN %l', $run_id, $activate_groups);
  }

  /**
   * Search current database and tries to find the all group_id user belongs to
   * for the run.
   *
   * @param int $uid    - UID of user
   * @param int $run_id - ID of the leaderboard run
   *
   * @return array
   */
  protected function _get_current_group_id($uid, $run_id) {
    $sql    = '
      SELECT [group_id]
      FROM [nn_leaderboard_group_run_users] 
      WHERE
        [run_id] = %i 
        AND [uid] = %i
      LIMIT 1;';
    $result = \dibi::query($sql, $run_id, $uid);

    return (array) $result->fetchAssoc('group_id');
  }
}