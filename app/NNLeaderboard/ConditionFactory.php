<?php namespace NNLeaderboard;

use NNCore\Tools\Tool;

/**
 * Class ConditionFactory
 *
 * @package NNLeaderboard
 */
class ConditionFactory {

  /**
   * Returns instance for Grouping class.
   *
   * @param $class_name
   *
   * @return GroupAbstracts
   */
  public static function get_obj($class_name) {

    $class_name      = Tool::underscore_to_camel($class_name);
    $class_namespace = '\\NNLeaderboard\\Conditions\\' . $class_name;

    return new $class_namespace();
  }
}