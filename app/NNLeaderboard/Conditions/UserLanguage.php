<?php namespace NNLeaderboard\Conditions;

/**
 * Class UserLanguage
 *
 * @package NNLeaderboard\Conditions
 */
class UserLanguage extends ConditionAbstract {

  /**
   * UserLanguage constructor.
   */
  public function __construct() {
    $this->_condition = 'user_language';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    $language = !empty($params['user_language']) ? $params['user_language'] : $this->_get_from_db($uid);

    return $this->_compare_values($language, $operator, $value);
  }

  /**
   * Returns user email from the db.
   *
   * @param int $uid - UID of the user
   *
   * @return NULL|string
   */
  private function _get_from_db($uid) {
    $request = \dibi::query('
      SELECT [language] 
      FROM [users]
      WHERE   
        [uid] = %i 
      %lmt
    ', $uid, 1);

    return $request->fetchSingle();
  }
}