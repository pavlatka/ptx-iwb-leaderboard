<?php namespace NNLeaderboard\Conditions;

/**
 * Class UserRole
 *
 * @package NNLeaderboard\Conditions
 */
class UserRole extends ConditionAbstract {

  /**
   * UserRole constructor.
   */
  public function __construct() {
    $this->_condition = 'user_role';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    $roles = $this->_get_from_db($uid);
    if (empty($roles)) {
      // Operator can be = or !=. We we don't have any roles, != must be true and = must be false
      return $operator == '!=';
    }

    // 2. Decision making.
    if ($operator == '=') {
      return array_key_exists($value, $roles);
    }

    return !array_key_exists($value, $roles);
  }

  /**
   * Returns user email from the db.
   *
   * @param int $uid - UID of the user
   *
   * @return NULL|string
   */
  private function _get_from_db($uid) {
    $request = \dibi::query('
      SELECT [rid] 
      FROM [users_roles]
      WHERE   
        [uid] = %i 
    ', $uid);

    return $request->fetchAssoc('rid');
  }
}