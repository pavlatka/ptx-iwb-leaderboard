<?php namespace NNLeaderboard\Conditions;

use NNCore\User;

/**
 * Class UserTestUser
 *
 * @package NNLeaderboard\Conditions
 */
class UserTestUser extends ConditionAbstract {

  /**
   * UserTestUser constructor.
   */
  public function __construct() {
    $this->_condition = 'user_test_user';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    $is_test = (int) User::is_test_user($uid);

    return $this->_compare_values($is_test, $operator, $value);
  }
}