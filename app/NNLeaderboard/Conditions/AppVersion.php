<?php namespace NNLeaderboard\Conditions;

/**
 * Class AppVersion
 *
 * @package NNLeaderboard\Conditions
 */
class AppVersion extends ConditionAbstract {

  /**
   * AppVersion constructor.
   */
  public function __construct() {
    $this->_condition = 'app_version';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    // 1. If we do not have information about device.
    if (!array_key_exists('app_version', $params)) {
      return FALSE;
    }

    return $this->_compare_values($params['app_version'], $operator, $value);
  }
}