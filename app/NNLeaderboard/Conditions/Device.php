<?php namespace NNLeaderboard\Conditions;

/**
 * Class Device
 *
 * @package NNLeaderboard\Conditions
 */
class Device extends ConditionAbstract {

  /**
   * Device constructor.
   */
  public function __construct() {
    $this->_condition = 'device';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    // 1. If we do not have information about device.
    if (!array_key_exists('device', $params)) {
      return FALSE;
    }

    return $this->_compare_values($params['device'], $operator, $value);
  }
}