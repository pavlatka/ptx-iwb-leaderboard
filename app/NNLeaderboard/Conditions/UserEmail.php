<?php namespace NNLeaderboard\Conditions;

/**
 * Class UserEmail
 *
 * @package NNLeaderboard\Conditions
 */
class UserEmail extends ConditionAbstract {

  /**
   * UserEmail constructor.
   */
  public function __construct() {
    $this->_condition = 'user_email';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    $mail = !empty($params['user_email']) ? $params['user_email'] : $this->_get_from_db($uid);

    return $this->_compare_values($mail, $operator, $value);
  }

  /**
   * Returns user email from the db.
   *
   * @param int $uid - UID of the user
   *
   * @return NULL|string
   */
  private function _get_from_db($uid) {
    $request = \dibi::query('
      SELECT [mail] 
      FROM [users]
      WHERE   
        [uid] = %i 
      %lmt
    ', $uid, 1);

    return $request->fetchSingle();
  }
}