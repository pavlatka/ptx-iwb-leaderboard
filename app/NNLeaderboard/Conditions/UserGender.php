<?php namespace NNLeaderboard\Conditions;

/**
 * Class UserGender
 *
 * @package NNLeaderboard\Conditions
 */
class UserGender extends ConditionAbstract {

  /**
   * UserGender constructor.
   */
  public function __construct() {
    $this->_condition = 'user_gender';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    $gender = !empty($params['user_gender']) ? $params['user_gender'] : $this->_get_from_db($uid);

    return $this->_compare_values($gender, $operator, $value);
  }

  /**
   * Returns gender from the db.
   *
   * @param int $uid - UID of the user
   *
   * @return NULL|string
   */
  private function _get_from_db($uid) {
    $request = \dibi::query('
      SELECT p.[field_gender_value] 
      FROM [node] n
      INNER JOIN [content_type_profile] p USING (nid, vid)
      WHERE   
        n.[uid] = %i 
      %lmt
    ', $uid, 1);

    return $request->fetchSingle();
  }
}