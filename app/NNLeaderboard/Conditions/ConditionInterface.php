<?php namespace NNLeaderboard\Conditions;

/**
 * Interface ConditionInterface
 *
 * @package NNLeaderboard\Conditions
 */
interface ConditionInterface {

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array());
}