<?php namespace NNLeaderboard\Conditions;

/**
 * Class ConditionAbstract
 *
 * @package NNLeaderboard\Conditions
 */
abstract class ConditionAbstract implements ConditionInterface {
  /**
   * @var
   */
  protected $_condition;

  /**
   * Compares $value with $check_with data using the $operator.
   *
   * @param mixed  $value      - value to be use in checking
   * @param string $operator   - operator which should be used
   * @param mixed  $check_with - mostly value specified in the condition
   *
   * @return bool
   */
  protected function _compare_values($value, $operator, $check_with) {
    switch ($operator) {
      case '>':
        return $value > $check_with;
        break;
      case '<':
        return $value < $check_with;
        break;
      case '>=':
        return $value >= $check_with;
        break;
      case '<=':
        return $value <= $check_with;
        break;
      case '=':
        return $value == $check_with;
        break;
      case '!=':
        return $value != $check_with;
        break;
      default:
        return FALSE;
    }
  }
}