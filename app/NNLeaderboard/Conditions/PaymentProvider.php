<?php namespace NNLeaderboard\Conditions;

/**
 * Class PaymentProvider
 *
 * @package NNLeaderboard\Conditions
 */
class PaymentProvider extends ConditionAbstract {

  /**
   * PaymentProvider constructor.
   */
  public function __construct() {
    $this->_condition = 'payment_provider';
  }

  /**
   * Checks whether the condition is valid.
   *
   * @param int    $uid      - UID of the user
   * @param string $operator - operator to be used
   * @param string $value    - value to be checked
   * @param array  $params   - additional params
   *
   * @return bool
   */
  public function validate($uid, $operator, $value, array $params = array()) {
    // 1. If we do not have information about device.
    $provider = !empty($params['payment_provider_id']) ? $params['payment_provider_id'] : $this->_get_from_db($uid);

    return $this->_compare_values($provider, $operator, $value);
  }

  /**
   * Returns device provider from the db.
   *
   * @param int $uid - UID of the user
   *
   * @return NULL|int
   */
  private function _get_from_db($uid) {
    $request = \dibi::query('
      SELECT [device_provider] 
      FROM [nn_logger_ads]
      WHERE 
        [action] LIKE %like~ 
        AND [users_uid] = %i 
      %lmt
    ', 'lead', $uid, 1);

    return $request->fetchSingle();
  }
}