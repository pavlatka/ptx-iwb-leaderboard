<?php namespace NNLeaderboard\Tools;

use NNCore\Tools\Tool;

class LeaderboardTool {

  /**
   * Returns start + end date for a lb cycle.
   *
   * @param string $renew_frequency - what's the time period of one cycle
   * @param string $date            - what's the start date
   *
   * @return array - start, end
   */
  public static function get_start_end_dates($renew_frequency, $date = NULL) {
    if ($date === NULL) {
      $date = date('Y-m-d');
    }

    switch ($renew_frequency) {
      case 'day':
        $start = $end = $date;

        return compact('start', 'end');
      case 'week':
        $monday_sunday = Tool::get_monday_sunday($date);

        return array('start' => $monday_sunday['monday'], 'end' => $monday_sunday['sunday']);
      case 'month':
        list($year, $month) = explode('-', date('Y-m', strtotime($date)));
        $start = $year . '-' . $month . '-01';
        $end   = $year . '-' . $month . '-' . date('t', strtotime($start . ' 00:00:00'));

        return compact('start', 'end');
      case 'year':
        $year  = date('Y', strtotime($date));
        $start = $year . '-01-01';
        $end   = $year . '-12-31';

        return compact('start', 'end');
      default:
        return array();
    }
  }
}