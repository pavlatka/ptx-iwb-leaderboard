<?php namespace NNLeaderboard;

use NNLeaderboard\Collections\ConditionsCollection;
use NNLeaderboard\Groups\GroupAbstract;
use NNLeaderboard\Tools\LeaderboardTool;

/**
 * Class Leaderboard
 *
 * @package NNLeaderboard
 */
class Leaderboard {

  /**
   * @var
   */
  private $_id;

  /**
   * @var
   */
  private $_data;

  /**
   * Holds information about the group.
   *
   * @var GroupAbstract
   */
  public $group;

  /**
   * List of names which should be excluded from leaderboard
   *
   * @var array
   */
  public static $exclude_names = array(
    "Unbenannter Nutzer",
    "Unnamed User",
    "Desconocido",
    "Utilisateur anonyme",
    "Usuário anônimo",
    "Utente anonimo",
    "Безымянный",
    "İsimsiz Kullanıcı"
  );

  /**
   * Leaderboard constructor.
   *
   * @param int $id - ID of the leaderboard
   *
   * @throws Exception
   * @throws \Dibi\Exception
   */
  public function __construct($id) {
    $this->_id = $id;
    $this->_load_data();
  }

  /**
   * Checks whether user can participate.\
   *
   * @param int   $uid    - UID of the user to be checked.
   * @param array $params - additional params
   *
   * @return bool
   */
  public function can_participate($uid, array $params = array()) {
    $conditions = ConditionsCollection::get_4_leaderboard($this->_id);
    $run_entry  = array_key_exists('run_has_entry', $params) ? $params['run_has_entry'] : FALSE;

    // 3. Check each condition.
    foreach ($conditions as $condition) {
      $mandatory = (bool) $condition['data']->flag_mandatory;

      if (!$run_entry || $mandatory) {
        if ($condition['condition']->validate($uid, $condition['data']->operator, $condition['data']->value, $params) === FALSE) {
          return FALSE;
        }
      }
    }

    // 3. If all the conditions are passed, welcome to leaderboard.
    return TRUE;
  }

  /**
   * Return current value of $this->_id
   *
   * @return int
   */
  public function get_id() {
    return (int) $this->_id;
  }

  /**
   * Return start, end date for current cycle.
   *
   * @param null|string $date - possible to send a date for another cycle.
   *
   * @return array - start, end
   */
  public function get_cycle_frame($date = NULL) {
    if ($date === NULL) {
      $date = date('Y-m-d');
    }

    return (array) LeaderboardTool::get_start_end_dates($this->_data->renew_frequency, $date);
  }

  /**
   * Returns data about current run.
   *
   * @return \Dibi\Row
   */
  public function get_data() {
    return $this->_data;
  }

  /**
   * Returns min_score for the leaderboard.
   *
   * @return int
   */
  public function get_min_score() {
    return (int) $this->_data->min_xp;
  }

  /**
   * Returns max_users for the leaderboard.
   *
   * @return int
   */
  public function get_max_users() {
    return (int) $this->_data->max_users;
  }

  /**
   * Returns min_users for the leaderboard.
   *
   * @return int
   */
  public function get_min_users() {
    return (int) $this->_data->min_users;
  }

  /**
   * Return name of the leaderboard.
   * Possible to ask for localized name if needed.
   *
   * @param null $locale
   *
   * @return string
   */
  public function get_name($locale = NULL) {
    if ($locale === NULL) {
      return $this->_data->name;
    }

    $locale_name = Localization::get('leaderboard', $this->_id, $locale);

    return !empty($locale_name) ? $locale_name : $this->_data->name;
  }

  /**
   * Loads data about the leaderboard.
   *
   * @throws Exception
   * @throws \Dibi\Exception
   */
  private function _load_data() {
    $sql         = '
      SELECT *
      FROM [nn_leaderboards]
      WHERE 
        [leaderboard_id] = %i
    ';
    $result      = \dibi::query($sql, $this->_id);
    $this->_data = $result->fetch();

    if (empty($this->_data)) {
      throw new Exception('Leaderboard - ' . $this->_id . ' - cannot be found');
    }

    $this->group = GroupFactory::get_obj($this->_data->group_class);
  }

}