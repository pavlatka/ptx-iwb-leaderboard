<?php namespace NNLeaderboard;

/**
 * Class Run
 *
 * @package NNLeaderboard
 */
class Run {
  /**
   * @var
   */
  private $_id;

  /**
   * @var
   */
  private $_data;

  /**
   * Link to the leaderboard object.
   *
   * @var Leaderboard
   */
  public $leaderboard;

  /**
   * Run constructor.
   *
   * @param int $id - ID of the run
   *
   * @throws Exception
   */
  public function __construct($id) {
    $this->_id = $id;
    $this->_load_data();
  }

  /**
   * Checks whether user can participate.
   *
   * @param int   $uid    - UID of the user to be checked.
   * @param array $params - additional params
   *
   * @return bool
   */
  public function can_participate($uid, array $params = array()) {
    // 1. Add extra information whether user has already entry for the run
    $params += array(
      'has_run' => $this->user_has_entry($uid)
    );

    // 2. Decide whether user can participate.
    return (bool) $this->leaderboard->can_participate($uid, $params);
  }

  /**
   * Returns data about current run.
   *
   * @return \Dibi\Row
   */
  public function get_data() {
    return $this->_data;
  }

  /**
   * Return ID of the run.
   *
   * @return int
   */
  public function get_id() {
    return (int) $this->_id;
  }

  /**
   * Returns how much XP the user has collected
   * for this run.
   *
   * @param int $uid - UID of the user
   *
   * @return int
   */
  public function get_xp_4_uid($uid) {
    $result = \dibi::query('
      SELECT IFNULL(run_xp, 0)
      FROM [nn_leaderboard_run_users]
      WHERE 
        [run_id] = %i 
        AND [uid] = %i
      %lmt
    ', $this->_id, $uid, 1);

    return (int) $result->fetchSingle();
  }

  /**
   * Checks whether user has already entry for
   * this run.
   *
   * @param int $uid - UID of the user to be checked.
   *
   * @return bool
   */
  public function user_has_entry($uid) {
    $result = \dibi::query('
      SELECT [run_id]
      FROM [nn_leaderboard_run_users]
      WHERE
        [uid] = %i
        AND [run_id] = %i
      %lmt
    ', $uid, $this->_id, 1);

    return $result->getRowCount() > 0;
  }

  /**
   * Loads data about the leaderboard.
   *
   * @throws Exception
   * @throws \Dibi\Exception
   */
  private function _load_data() {
    $sql         = '
      SELECT *
      FROM [nn_leaderboard_runs]
      WHERE 
        [run_id] = %i
    ';
    $result      = \dibi::query($sql, $this->_id);
    $this->_data = $result->fetch();

    if (empty($this->_data)) {
      throw new Exception('Leaderboard Run - ' . $this->_id . ' - cannot be found');
    }

    $this->leaderboard = new Leaderboard($this->_data->leaderboard_id);
  }
}