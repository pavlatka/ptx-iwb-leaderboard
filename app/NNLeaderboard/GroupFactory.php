<?php namespace NNLeaderboard;

use NNLeaderboard\Groups\NoGroup;
use NNLeaderboard\Groups\LocationCountry;

/**
 * Class GroupFactory
 *
 * @package NNLeaderboard
 */
class GroupFactory {

  /**
   * Returns instance for Grouping class.
   *
   * @param $class_name
   *
   * @return GroupAbstracts
   */
  public static function get_obj($class_name) {
    switch ($class_name) {
      case 'no_group':
        return new NoGroup();
        break;
      case 'location_country':
        return new LocationCountry();
        break;
      default:
        return NULL;
    }
  }
}