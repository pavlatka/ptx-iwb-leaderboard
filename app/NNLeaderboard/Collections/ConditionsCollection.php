<?php namespace NNLeaderboard\Collections;
use NNLeaderboard\ConditionFactory;

/**
 * Class ConditionsCollection
 *
 * @package NNLeaderboard\Collections
 */
class ConditionsCollection {

  /**
   * Returns collection of the conditions for the
   * leaderboard.
   *
   * @param int $leaderboard_id - ID of the leaderboard
   *
   * @return array
   */
  public static function get_4_leaderboard($leaderboard_id) {
    $result = \dibi::query('
      SELECT *
      FROM [nn_leaderboard_conditions]
      WHERE
        [leaderboard_id] = %i
      ORDER BY [condition_id] ASC
    ', $leaderboard_id);

    $collection = array();
    foreach ($result->getIterator() as $row) {
      $collection[$row->condition_id] = array(
        'data'      => $row,
        'condition' => ConditionFactory::get_obj($row->condition_tag)
      );
    }

    return (array) $collection;
  }
}