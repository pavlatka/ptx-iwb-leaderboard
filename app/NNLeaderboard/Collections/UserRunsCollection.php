<?php namespace NNLeaderboard\Collections;

use NNLeaderboard\Leaderboard;
use NNLeaderboard\Run;

/**
 * Class UserRunsCollection
 * Handles runs for the user.
 *
 * @package NNLeaderboard\Collections
 */
class UserRunsCollection {

  /**
   * UID of the user who is requesting runs
   *
   * @var int
   */
  private $_uid;

  /**
   * Locale we work in
   *
   * @var
   */
  private $_locale;

  /**
   * Additional params
   *
   * @var
   */
  private $_params = array();

  /**
   * UserRun constructor.
   *
   * @param int    $uid    - UID of the user
   * @param string $locale - possible to affect final translations
   * @param array  $params - additional params
   */
  public function __construct($uid, $locale = NULL, array $params = array()) {
    $this->_uid    = (int) $uid;
    $this->_locale = $locale;
    $this->_params = array_merge($this->_params, $params);
  }

  /**
   * Return active runs for the user.
   *
   * @return array
   */
  public function get_active_runs() {
    // 1. Load all current runs for the user.
    $current_runs = $this->get_current_runs();
    if (empty($current_runs)) {
      return array();
    }

    $active_runs = array();
    /* @var Run $run ; */
    foreach ($current_runs as $run) {
      // 2a. Variables.
      $run_id           = $run->get_id();
      $run_data         = $run->get_data();
      $leaderboard_data = $run->leaderboard->get_data();

      // 2b. Load active groups.
      $active_groups = $run->leaderboard->group->get_active_groups($run_id, $this->_uid, TRUE, TRUE, $this->_locale);
      if (empty($active_groups)) {
        continue;
      }

      // 2c. Prepare array to return
      $return_data = array(
        'run_id'       => (int) $run_id,
        'run_start'    => strtotime($run_data->period_start->format('Y-m-d') . ' 00:00:00'),
        'run_end'      => strtotime($run_data->period_end->format('Y-m-d') . ' 23:59:59'),
        'image_url'    => $leaderboard_data->image_url,
        'image_width'  => (int) $leaderboard_data->image_width,
        'image_height' => (int) $leaderboard_data->image_height,
        'max_users'    => (int) $leaderboard_data->max_users,
        'min_users'    => (int) $leaderboard_data->min_users,
        'min_score'    => (int) $leaderboard_data->min_xp
      );

      foreach ($active_groups as $active_group) {
        $group_data = array(
          'id'         => $active_group['id'],
          'weight'     => $active_group['group_weight'],
          'name'       => $run->leaderboard->get_name($this->_locale) . ' ' . $active_group['name'],
          'short_name' => $active_group['name']
        );

        if (!empty($active_group['image_url'])) {
          $group_data += array(
            'image_url'    => $active_group['image_url'],
            'image_width'  => $active_group['image_width'],
            'image_height' => $active_group['image_height']
          );
        }

        $active_runs[] = array_merge($return_data, $group_data);
      }
    }

    // 3. Return what he have collected.
    return (array) $active_runs;
  }

  /**
   * Returns information about run for the user.
   *
   * @return array
   */
  public function get_current_runs() {
    // 1. Load active leaderboards.
    $leaderboards = LeaderboardsCollection::get_all(array(
      'flag_active' => 1
    ));
    if (empty($leaderboards)) {
      return array();
    }

    /** @var Leaderboard $leaderboard */
    // 2. Try to find run for each leaderboard
    $runs = array();
    foreach ($leaderboards as $leaderboard) {

      // 2a. Find information about the run.
      $cycle_frame    = $leaderboard->get_cycle_frame();
      $leaderboard_id = $leaderboard->get_id();
      $run            = RunsCollection::get_4_lb_cycle($leaderboard_id, $cycle_frame['start'], $cycle_frame['end']);
      if ($run === NULL) {
        $run = RunsCollection::create_4_lb($leaderboard_id, $cycle_frame['start'], $cycle_frame['end']);
      }

      // 2b. If we cannot figure out a run, skip it.
      if ($run === NULL) {
        continue;
      }

      // 2a. Check whether user can participate.
      if ($run->can_participate($this->_uid, $this->_params) === FALSE) {
        continue;
      }

      // 2d. Check whether user belongs to any group.
      if ($run->leaderboard->group->has_group($this->_uid, $run->get_id()) === FALSE) {
        continue;
      }

      $runs[$run->get_id()] = $run;
    }

    return (array) $runs;
  }

}