<?php namespace NNLeaderboard\Collections;

/**
 * Class UserTrophiesCollection
 *
 * @package NNLeaderboard\Collections
 */
class UserTrophiesCollection {

  /**
   * Returns list of trophies for uid.
   *
   * @param int $uid            - UID of the user
   * @param int $leaderboard_id - ID of the leaderboard we want to know trophies for
   * @param int $group_id       - ID of group we want to know trophies for
   *
   * @return array
   */
  public static function get_4_uid($uid, $leaderboard_id = NULL, $group_id = NULL) {
    // 1. Load data from db.
    $result = \dibi::query('
      SELECT 
        [trophy_id], SUM([amount]) amount
      FROM [nn_leaderboard_trophy_users_score] 
      WHERE
        [uid] = %i
    ', $uid,
      '%if', $leaderboard_id !== NULL, ' AND [leaderboard_id] = %i', $leaderboard_id, '%end',
      '%if', $group_id !== NULL, ' AND [group_id] = %i', $group_id, '%end',
      ' GROUP BY [trophy_id]');

    // 2. Build array.
    $trophies = array();
    foreach ($result->getIterator() as $trophy) {
      $trophies[] = $trophy->toArray();
    }

    // 3. Return what we have found.
    return (array) $trophies;
  }
}