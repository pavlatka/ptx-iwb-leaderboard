<?php namespace NNLeaderboard\Collections;
use NNCore\Tools\DbTool;

/**
 * Class Leaderboard
 *
 * @package NNLeaderboard\Collections
 */
class LeaderboardsCollection {

  /**
   * Returns list of leaderboards for specific params.
   *
   * @param array $params - possible to affect result
   *
   * @return array
   *
   * @throws \NNLeaderboard\Exception
   * @throws \Dibi\Exception
   */
  public static function get_all(array $params = array()) {
    $sql = '
      SELECT [leaderboard_id]
      FROM [nn_leaderboards]
      WHERE 1 ';

    $fields = array('leaderboard_id', 'flag_active', 'flag_autorenew');
    foreach ($fields as $field) {
      if (array_key_exists($field, $params)) {
        $sql .= ' AND ' . DbTool::extend_conditions($field, $params[$field]);
      }
    }

    if (array_key_exists('group_by', $params)) {
      $sql .= ' GROUP BY ' . $params['group_by'];
    }

    if (array_key_exists('order_by', $params)) {
      $sql .= ' ORDER BY ' . $params['order_by'];
    }

    if (array_key_exists('limit', $params)) {
      $sql .= ' LIMIT ' . (int) $params['limit'];
    }

    if (array_key_exists('offset', $params)) {
      $sql .= ' OFFSET ' . (int) $params['offset'];
    }

    $result       = \dibi::query($sql);
    $leaderboards = $result->fetchAll();

    $collections = array();
    foreach ($leaderboards as $leaderboard) {
      $collections[] = new \NNLeaderboard\Leaderboard($leaderboard->leaderboard_id);
    }

    return (array) $collections;
  }
}