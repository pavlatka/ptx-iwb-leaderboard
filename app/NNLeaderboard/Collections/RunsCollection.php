<?php namespace NNLeaderboard\Collections;

use NNCore\DbTool;
use NNLeaderboard\Run;

/**
 * Class RunsCollection
 *
 * @package NNLeaderboard\Collections
 */
class RunsCollection {

  /**
   * Create new run for a leaderboard and specific cycle frame.
   *
   * @param int    $leaderboard_id - ID of the leaderboard
   * @param string $cycle_start    - start date, in YYYY-mm-dd format
   * @param string $cycle_end      - end date, in YYYY-mm-dd format
   *
   * @return Run|null
   */
  public static function create_4_lb($leaderboard_id, $cycle_start, $cycle_end) {

    $data = array(
      'leaderboard_id' => $leaderboard_id,
      'period_start'   => $cycle_start,
      'period_end'     => $cycle_end
    );

    if (!\dibi::query('INSERT INTO [nn_leaderboard_runs]', $data)) {
      return NULL;
    }

    $run_id = DbTool::get_last_insert_id('nn_leaderboard_runs');
    if (empty($run_id)) {
      return NULL;
    }

    return new Run($run_id);
  }

  /**
   * Checks whether run has relation with the group.
   *
   * @param int $run_id   - ID of the run
   * @param int $group_id - ID of the group
   *
   * @return bool
   */
  public static function run_has_group($run_id, $group_id) {
    $result = \dibi::query('
      SELECT 1
      FROM [nn_leaderboard_group_runs]
      WHERE 
        [run_id] = %i
        AND [group_id] = %i
      LIMIT 1
    ', $run_id, $group_id);

    return $result->getRowCount() > 0;
  }

  /**
   * Returns runs for specific leaderboard and cycle.
   *
   * @param int    $leaderboard_id - ID of the leaderboard
   * @param string $cycle_start    - start date, in YYYY-mm-dd format
   * @param string $cycle_end      - end date, in YYYY-mm-dd format
   *
   * @return null|Run
   */
  public static function get_4_lb_cycle($leaderboard_id, $cycle_start, $cycle_end) {
    $sql    = '
      SELECT [run_id]
      FROM [nn_leaderboard_runs]
      WHERE
        [leaderboard_id] = %i
        AND [period_start] = %d
        AND [period_end] = %d
      ORDER BY [period_start] ASC
      LIMIT 1 OFFSET 0
    ';
    $result = \dibi::query($sql, $leaderboard_id, $cycle_start, $cycle_end);
    $run_id = $result->fetchSingle();

    if (empty($run_id)) {
      return NULL;
    }

    return new Run($run_id);
  }

}