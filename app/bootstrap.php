<?php

require_once __DIR__ . '/../../../vendor/autoload.php';

/* CONSTANTS */
define('MEMCACHIER_USERNAME', '034754');
define('MEMCACHIER_PASSWORD', 'c7b91ec864934cf16341f0d8993ade7d');
define('MEMCACHIER_SERVERS', '121573.034754.eu-west-2.heroku.prod.memcachier.com');
define('MEMCACHIER_PORT', 11211);

/* CONFIGURE DIBI DB LAYER */
dibi::connect(array(
  'driver'   => 'mysql',
  'host'     => 'localhost',
  'username' => 'root',
  'password' => '',
  'database' => 'dep5sa2vteb',
  'charset'  => 'utf8',
));

/* CONFIGURE SLIM FRAMEWORK */
$config = array(
  'settings' => array(
    'displayErrorDetails'    => TRUE,
    'addContentLengthHeader' => FALSE
  )
);
$app    = new Slim\App($config);

require_once __DIR__ . '/../app/routes.php';

$app->run(FALSE);
