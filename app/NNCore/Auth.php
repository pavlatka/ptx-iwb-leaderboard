<?php namespace NNCore;

/**
 * Class Auth
 *
 * @package NNCore
 */
class Auth {

  /**
   * Checks whether credentials are valid.
   *
   * @param int    $uid - UID of the user
   * @param string $pw  - password of the user
   *
   * @return bool
   */
  public static function authenticate($uid, $pw) {
    $result = \dibi::query('
      SELECT [uid]
      FROM [users]
      WHERE 
        [uid] = %i
        AND [pass] = %s
      LIMIT 1
    ', $uid, $pw);

    return $result->getRowCount() > 0;
  }
}