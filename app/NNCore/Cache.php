<?php namespace NNCore;

use Desarrolla2\Cache\Adapter\Memcached;
use \Memcached as Backend;

/**
 * Class Cache
 *
 * @package NNCore
 */
class Cache {

  /**
   * @var
   */
  private $_cache;

  /**
   * @var
   */
  protected static $_instance;

  /**
   * Singleton implementation.
   *
   * @return Cache
   */
  public static function get_instance() {
    if (self::$_instance === NULL) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }

  /**
   *
   */
  protected function __clone() {
  }

  /**
   * Cache constructor.
   */
  protected function __construct() {
    $backend = new Backend();
    $backend->addServer(MEMCACHIER_SERVERS, MEMCACHIER_PORT);
    $backend->setOption(Backend::OPT_BINARY_PROTOCOL, TRUE);
    $backend->setSaslAuthData(MEMCACHIER_USERNAME, MEMCACHIER_PASSWORD);

    $adapter      = new Memcached($backend);
    $this->_cache = new \Desarrolla2\Cache\Cache($adapter);
  }

  /**
   * Returns value from cache.
   *
   * @param $key
   *
   * @return mixed
   */
  public static function get($key) {
    return self::get_instance()->_cache->get($key);
  }

  /**
   * Sets new cache entry
   *
   * @param string $key   - key which we can use to retrieve data
   * @param string $value - value which should be saved
   * @param int    $valid - how long the cache should be kept, in ms
   */
  public static function set($key, $value, $valid = 3600) {
    self::get_instance()->_cache->set($key, $value, $valid);
  }
}

