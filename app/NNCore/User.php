<?php namespace NNCore;

use NNCore\Tools\Tool;

/**
 * Class User
 *
 * @package NNCore
 */
class User {

  const ROLE_ID_TEST_USER = 27;

  /**
   * Returns profiles for users specified in $uids param
   *
   * @param array $uids          - list of UIDs for user we want profile for
   * @param array $params        - additional params
   * @param array $encode_fields - which fields should be urlencoded before returning.
   *
   * @return array
   */
  public static function get_profile_4_uids(array $uids, array $params = array(), array $encode_fields = array()) {
    // 0. If we do not have data, we are done.
    if (empty($uids)) {
      return array();
    }

    // 1. Prepare variables.
    $cursor     = 0;
    $step       = 100;
    $profiles   = array();
    $count_uids = count($uids);

    // 2. Prepare stats.
    $stat_name         = 'xp';
    $stat_component_id = 2;
    if (!empty($params['service_version']) && $params['service_version'] >= 10) {
      $stat_name         = 'level';
      $stat_component_id = 4;
    }

    // 3. Just in case, do it in batch.
    do {
      // 3a. Slice array to max amount.
      $uids_part = array_slice($uids, $cursor, $step);

      // 3b. Load data from db.
      $result = \dibi::query('
        SELECT
          u.[uid], 
          u.[mail], 
          u.[name], 
          p.[field_realname_value] real_name, 
          u.[picture] picture_profile,
          IFNULL(s.value_int, 0) %s,
          f.[filepath]
        FROM [users] u
        JOIN [node] n1 ON 
          u.[uid] = n1.uid 
          AND n1.[type] = "profile"
        JOIN [content_type_profile] p USING(nid, vid)    
        LEFT JOIN [files] f ON 
          f.[uid] = u.[uid] 
          AND f.[fid] = p.[field_profilepicture_fid]
        LEFT JOIN [nn_user_stats] s ON
          s.[uid] = u.uid 
          AND s.[stat_type] = %i 
          AND s.[stat_id] = %i 
          AND s.[stat_component_id] = %i
        WHERE 
          u.uid IN %l
        GROUP BY u.[uid]
      ', $stat_name, 4, 5, $stat_component_id, $uids_part);

      // 3c. Extend profiles array.
      foreach ($result->getIterator() as $user) {
        // Complete link for picture
        $user = $user->toArray();
        if ($user['filepath'] !== NULL) {
          $user['picture_profile'] = $user['filepath'];
        }
        elseif ($user['picture_profile'] == 'sites/default/files/userpic3.jpg') {
          $user['picture_profile'] = 'http://neuronation.herokuapp.com/' . $user['picture_profile'];
        }

        unset($user['filepath']);
        $profiles[$user['uid']] = $user;
      }

      // 3d. Let's move to next step.
      $cursor += $step;
    } while ($cursor < $count_uids);

    // 4. If we do not have any profiles, return empty array
    if (empty($profiles)) {
      return array();
    }

    // If we need to encode some fields before sending, now it's time.
    if (!empty($encode_fields)) {
      $profiles = Tool::encode_fields($profiles, $encode_fields);
    }

    // Return profiles.
    return $profiles;
  }

  /**
   * Returns information about thumbnail for the user
   *
   * @param int $uid - UID of the user we want thumbnail for
   *
   * @return null|string
   */
  public static function get_thumbnail($uid) {
    static $_cache = array();

    // 1. Check db only in case we do not have the picture
    if (!array_key_exists($uid, $_cache)) {
      $result       = \dibi::query('
        SELECT u.[picture]
        FROM [users] u
        WHERE 
          u.[uid] = %i
        %lmt
      ', $uid, 1);
      $_cache[$uid] = $result->fetchSingle();
    }

    return $_cache[$uid];
  }

  /**
   * Checks whether the user is a test user.
   *
   * @param int $uid - UID of the user to be checked.
   *
   * @return bool
   */
  public static function is_test_user($uid) {
    $result = \dibi::query('
      SELECT [rid]
      FROM [users_roles]
      WHERE
        uid = %i
        AND rid = %i
    ', $uid, self::RID_TEST_USER);

    return $result->getRowCount() > 0;
  }
}