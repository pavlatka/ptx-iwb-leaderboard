<?php namespace NNCore\Tools;

/**
 * Class DbTool
 *
 * @package NNCore
 */
class DbTool {

  /**
   * Returns last inserted ID for specific table.
   *
   * @param string $table - name of the table
   *
   * @return int
   */
  public static function get_last_insert_id($table) {
    $result = \dibi::query('
      SELECT LAST_INSERT_ID() 
      FROM %n
    ', $table);

    return (int) $result->fetchSingle();
  }

  /**
   * Extends conditions for the column and its value.
   *
   * @param string            $column - name of the column
   * @param int|string|array $value  - its value
   *
   * @return int|string
   */
  public static function extend_conditions($column, $value) {
    if (empty($value) && !is_numeric($value)) {
      return 1;
    }

    if (!is_array($value)) {
      if (is_numeric($value)) {
        return $column . ' = ' . $value;
      }

      return $column . ' = "' . $value . '"';
    }

    $string = NULL;
    foreach ($value as $val) {
      $string .= "'{$val}',";
    }

    return $column . ' IN (' . trim(substr($string, 0, -1)) . ')';
  }
}