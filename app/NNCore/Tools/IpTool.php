<?php namespace NNCore\Tools;

/**
 * Class IpTool
 *
 * @package NNCore\Tools
 */
class IpTool {

  /**
   * Returns geo data from IP address.
   *
   * @param null|string $ip - IP address
   *
   * @return object
   */
  public static function get_geo_data($ip = NULL) {
    // 0. Complete IP if needed.
    if ($ip === NULL) {
      $ip = self::ip_address();
    }

    // 1. Figure out what ip type we are dealing with.
    $ip_type = self::get_ip_type($ip);
    if (!in_array($ip_type, array('ipv4', 'ipv6'), FALSE)) {
      return new stdClass();
    }

    // 2. Load data for IP V4 Address.
    if ($ip_type == 'ipv4') {
      $result = \dibi::query(
        'SELECT *
        FROM [nn_ip_ip2location_v4]
        WHERE
         [ip_to] >= INET_ATON(%s)
        ORDER BY [ip_to] ASC
        %lmt
      ', $ip, 1);

      return $result->fetch();
    }

    // 2. Load data for IP V6 Address.
    $result = \dibi::query('
      SELECT *
      FROM [nn_ip_ip2location_v6]
      WHERE 
        [ip_to] >= %s
      ORDER BY [ip_to] ASC
      %lmt
    ', self::ip_v6_string_2_int($ip));

    return $result->fetch();
  }

  /**
   * Returns IP type - ipv4 or ipv6
   *
   * @param string $ip - IP address to be checked
   *
   * @return null|string - ipv4 / ipv6
   */
  public static function get_ip_type($ip) {
    // 1. IP version 4.
    if (ip2long($ip) !== FALSE) {
      return 'ipv4';
    }

    // 2. IP version 6.
    if (preg_match('/^[0-9a-fA-F:]+$/', $ip) && @inet_pton($ip) !== FALSE) {
      return 'ipv6';
    }

    // 3. By default NULL
    return NULL;
  }

  /**
   * Return localized name for the area.
   *
   * @param string $type   - name of the area - e.g. country, city, region
   * @param string $string - english name
   * @param string $locale - 2 letters locale, e.g. EN for english
   *
   * @return string localized name
   */
  public static function get_localize_name($type, $string, $locale = NULL) {
    $locale = strtoupper($locale);

    if ($type == 'country') {
      return self::get_localize_name_country($string, $locale);
    }

    return $string;
  }

  /**
   * Returns localized name for the country.
   *
   * @param string $code   - code to be translated
   * @param string $locale - language code
   *
   * @return string
   */
  public static function get_localize_name_country($code, $locale) {
    $result = \dibi::query('
      SELECT [country_name]
      FROM [nn_ip_country_localizations]
      WHERE 
        [country_alpha2_code] = %s 
        AND [lang] = %s
      %lmt
    ', $code . $locale);

    $country_name = $result->fetchSingle();

    return !empty($country_name) ? $country_name : $code;
  }

  /**
   * Returns current IP address.
   *
   * @return mixed|null
   */
  public static function ip_address() {
    static $ip_address = NULL;

    if (!isset($ip_address)) {
      $ip_address = $_SERVER['REMOTE_ADDR'];
      if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        list($ip_address) = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
      }
    }

    return $ip_address;
  }

  /**
   * Returns IP number from IP string.
   *
   * @param string $ip - String with ip address, e.g. 195.226.114.18
   *
   * @return int
   */
  public static function ip_v4_string_2_int($ip) {
    // 1. If we do not have IP address, we can't do pretty much.
    if (empty($ip)) {
      return 0;
    }

    // 2. Count number.
    $ips = explode('.', $ip);

    return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
  }

  /**
   * Converts IP V6 address from string to int.
   *
   * @param string $ip
   *
   * @return int|string
   */
  public static function ip_v6_string_2_int($ip) {
    $bin  = '';
    $ip_n = inet_pton($ip);
    for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
      $bin = sprintf('%08b', ord($ip_n[$bit])) . $bin;
    }

    if (function_exists('gmp_init')) {
      return gmp_strval(gmp_init($bin, 2), 10);
    }

    if (function_exists('bcadd')) {
      $dec  = '0';
      $iMax = strlen($bin);
      for ($i = 0; $i < $iMax; $i++) {
        $dec = bcmul($dec, '2', 0);
        $dec = bcadd($dec, $bin[$i], 0);
      }

      return $dec;
    }

    return 0;
  }
}