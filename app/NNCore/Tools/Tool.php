<?php namespace NNCore\Tools;

class Tool {
  /**
   * Make sure that we have always same set of locales.
   *
   * @param string $locale - original locale
   *
   * @return string - fixed locale
   */
  public static function fix_locale($locale) {
    switch ($locale) {
      case 'en-us':
        return 'en';
        break;
      case 'pt-br':
        return 'pt';
        break;
      default:
        return $locale;
        break;
    }
  }

  /**
   * Return dates of monday and sunday for every date given.
   *
   * @param string $date - date within a week
   *
   * @return array - monday, sunday
   */
  public static function get_monday_sunday($date = NULL) {
    if ($date === NULL) {
      $date = date('Y-m-d');
    }
    $day_number = date('N', strtotime($date)); // 1 (for Monday) through 7 (for Sunday)

    $monday_key = '-6 days';
    $sunday_key = NULL;
    if ($day_number !== 7) {
      $monday_key = '-' . ($day_number - 1) . ' days';
      $sunday_key = '+' . (7 - $day_number) . ' days';
    }

    $monday = date('Y-m-d', strtotime($date . $monday_key));
    $sunday = date('Y-m-d', strtotime($date . $sunday_key));

    return compact('monday', 'sunday');
  }

  /**
   * Adds urlencode to specific columns.
   *
   * @param array $data
   * @param array $columns
   *
   * @return array
   */
  public static function encode_fields($data, array $columns = array('name', 'real_name')) {
    if (!is_array($data) || empty($columns)) {
      return $data;
    }

    foreach ($data as $key => &$value) {
      if (is_array($value)) {
        $value = self::encode_fields($value, $columns);
      }
      elseif (in_array($key, $columns, FALSE)) {
        $value = urlencode(strtr($value, array('"' => "'")));
      }
    }

    return (array) $data;
  }

  /**
   * Converts string from underscore format to camel style format.
   *
   * @param string $string - string in underscore format
   *
   * @return string
   */
  public static function underscore_to_camel($string) {
    return str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
  }
}